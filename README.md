# Our e-commerce store

## our e-commerce store is store to select category and show products based on that category and you can filter the product by color,max and min price, and average rating.

## i used reactJs as JavaScript library for building user interfaces, and matrialUi to get foundational and advanced components, enabling you to build your own design system and develop React applications faster.

### the solution of the problem is create a web app using library such as ReactJs so the solution focuses on front-end

There are many frameworks and libraries available for frontend development. Not all of them are good. React is one of the most popular and widely used libraries (it’s not a framework) for frontend development.

To give you a gentle introduction, React is an open-source JavaScript library used for frontend development, which was developed by Facebook. Its component-based library lets you build high-quality user-interfaces for web apps.

This library allows you to place HTML code inside JavaScript and it works with Virtual DOM.

# It:

1- Easy to learn

2- Rich user-interfaces

3-Faster development

4-Trusted by great companies

5-Strong community support.

## I left the filter colors based on checkbox because the lack of documentaion of jsonServer and this is the first time i used this package, i think if i spend more time I can make this project better in terms of design

# How to Run The Project:

after you clone the project just do yarn and after finched the process of install node modules you can now start the project by yarn start
