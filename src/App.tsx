import React from "react";
import "./App.css";
import { HomePage } from "pages";
import { Footer } from "components";

const Routes = () => {
  return (
    <div
      style={{ display: "flex", flexDirection: "column", minHeight: "100vh" }}
    >
      <HomePage />
      <Footer />
    </div>
  );
};
const App = () => {
  return <Routes />;
};
export default App;
