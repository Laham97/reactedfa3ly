import api from "../api";

const CATEGORY = "category";

export const mockApiC = {
  get: () => api.get(`${CATEGORY}`),
};
