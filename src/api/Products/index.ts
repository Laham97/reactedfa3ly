import api from "../api";

const PRODUCTS = "product";

export const mockApiP = {
  get: ({ params }: any) => api.get(`${PRODUCTS}`, params),
};
