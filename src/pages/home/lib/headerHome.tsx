import React from "react";
import { Typography } from "@mui/material";
import "./home.css";
export const HeaderHome = () => {
  return (
    <>
      <Typography variant="h4" gutterBottom className="textHeader">
        Our e-commerce Store
      </Typography>
      <Typography variant="h6" gutterBottom className="textChoose">
        chooose one of our categories from below
      </Typography>
    </>
  );
};
