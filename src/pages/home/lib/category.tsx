import React from "react";
import { Stack, Paper, Typography, Box } from "@mui/material";
import { setCategoire, selectSelectedCat } from "store";
import { useSelector, useDispatch } from "react-redux";

interface Props {
  data: any;
}
interface Item {
  id: number;
  name: string;
  image: string;
}
export const Categories: React.FC<Props> = ({ data }) => {
  const dispatch = useDispatch();
  const selectedCate = useSelector(selectSelectedCat);

  const selectCategooire = (cate: any) => {
    dispatch(setCategoire(cate));
  };
  return (
    <Stack
      direction={{ xs: "column", sm: "column", md: "row" }}
      spacing={{ xs: 1, sm: 2, md: 4 }}
      alignItems="center"
      justifyContent="center"
    >
      {data?.map((item: Item, key: any) => (
        <div key={key} onClick={() => selectCategooire(item)}>
          <Paper
            sx={{
              m: 1,
              width: 150,
              height: 150,
              alignItems: "center",
              justifyContent: "center",
              display: "flex",
              borderRadius: 10,
            }}
            elevation={3}
          >
            <Box
              sx={{
                backgroundImage: "url('" + item?.image + "')",
                backgroundRepeat: "no-repeat",
                width: 150,
                height: 150,
                borderRadius: 10,
                alignItems: "center",
                justifyContent: "center",
                display: "flex",
                backgroundSize: "cover",
                backgroundPosition: "center",
                ":hover": {
                  cursor: "pointer",
                },
              }}
            >
              <Typography
                sx={{
                  color:
                    selectedCate?.name === item?.name ? "royalblue" : "white",
                  fontSize: 20,
                  ":hover": {
                    color: "royalblue",
                  },
                }}
                variant="h5"
              >
                {item?.name}
              </Typography>
            </Box>
          </Paper>
        </div>
      ))}
    </Stack>
  );
};
