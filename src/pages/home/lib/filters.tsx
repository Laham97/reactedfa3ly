import React from "react";
import { Box, Typography } from "@mui/material";
import { PriceRange, ColorPicker, RadioGroupRating } from "components";
import ClearIcon from "@mui/icons-material/Clear";

interface Props {
  value: number[];
  handleChange: any;
  min?: number;
  max?: number;
  data: any;
  colorText: string;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onSub: () => void;
  values: number | null;
  onChangeStar: (event: any, newValue: any | null) => void;
  fromValue: string;
  onChangeFrom: (event: React.ChangeEvent<HTMLInputElement>) => void;
  toValue: string;
  onChangeTo: (event: React.ChangeEvent<HTMLInputElement>) => void;
  submitPrice: () => void;
  onSubmitSlider: () => void;
  clearRate: () => void;
}

export const Filters: React.FC<Props> = ({
  value,
  handleChange,
  min,
  max,
  colorText,
  onChange,
  data,
  onSub,
  values,
  onChangeStar,
  fromValue,
  onChangeFrom,
  toValue,
  onChangeTo,
  submitPrice,
  onSubmitSlider,
  clearRate,
}) => {
  const uniqueColors = data
    ?.map((item: any) => item.color)
    .filter(
      (value: any, index: any, self: any) => self.indexOf(value) === index
    );
  return (
    <Box
      sx={{
        borderRadius: 2,
        backgroundColor: "#252525",
        alignItems: "center",
        display: "flex",
        flexDirection: "column",
      }}
    >
      <PriceRange
        labelOne="From $"
        labelTow="To $"
        variantOne="outlined"
        variantTow="outlined"
        max={max}
        min={min}
        onSubmitSlider={onSubmitSlider}
        submitPrice={submitPrice}
        fromValue={fromValue}
        onChangeFrom={onChangeFrom}
        toValue={toValue}
        onChangeTo={onChangeTo}
        handleChange={handleChange}
        value={value}
      />
      <ClearIcon
        sx={{
          alignSelf: "flex-start",
          margin: 2,
          cursor: "pointer",
          ":hover": {
            color: "red",
          },
        }}
      />
      <Typography>Colors</Typography>
      <ColorPicker
        data={uniqueColors}
        colorText={colorText}
        onChange={onChange}
        onSub={onSub}
      />
      <ClearIcon
        sx={{
          alignSelf: "flex-start",
          margin: 2,
          cursor: "pointer",
          ":hover": {
            color: "green",
          },
        }}
      />
      <Typography>AVG Rating</Typography>
      <Box sx={{ margin: 4 }}>
        <RadioGroupRating value={values} onChangeStar={onChangeStar} />
      </Box>

      <ClearIcon
        sx={{
          alignSelf: "flex-start",
          margin: 2,
          cursor: "pointer",
          ":hover": {
            color: "yellow",
          },
        }}
        onClick={clearRate}
      />
    </Box>
  );
};
