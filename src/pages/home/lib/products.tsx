import React from "react";
import { Grid, Box, Pagination } from "@mui/material";
import { CardProducts } from "components";
import { Filters } from "./filters";

interface Props {
  data: any;
  colorText: string;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onSub: () => void;
  values: number | null;
  onChangeStar: (event: any, newValue: any | null) => void;
  page: number;
  handleChangePage: (event: React.ChangeEvent<unknown>, value: number) => void;
  count: number;
  fromValue: string;
  onChangeFrom: (event: React.ChangeEvent<HTMLInputElement>) => void;
  toValue: string;
  onChangeTo: (event: React.ChangeEvent<HTMLInputElement>) => void;
  submitPrice: () => void;
  valueMinMax: number[];
  handleChange: any;
  min?: number;
  max?: number;
  onSubmitSlider: () => void;
  clearRate: () => void;
}

export const Products: React.FC<Props> = ({
  data,
  onChange,
  colorText,
  onSub,
  values,
  onChangeStar,
  page,
  handleChangePage,
  count,
  fromValue,
  onChangeFrom,
  toValue,
  onChangeTo,
  submitPrice,
  valueMinMax,
  handleChange,
  min,
  max,
  onSubmitSlider,
  clearRate,
}) => {
  return (
    <Box sx={{ margin: "2rem" }}>
      <Grid container spacing={2} justifyContent="space-between">
        <Grid item xs={false} sm={12} md={6} lg={5}>
          <Filters
            value={valueMinMax}
            handleChange={handleChange}
            max={1000}
            onSub={onSub}
            onSubmitSlider={onSubmitSlider}
            data={data}
            submitPrice={submitPrice}
            onChangeFrom={onChangeFrom}
            fromValue={fromValue}
            onChangeTo={onChangeTo}
            toValue={toValue}
            onChangeStar={onChangeStar}
            values={values}
            onChange={onChange}
            clearRate={clearRate}
            colorText={colorText}
          />
        </Grid>
        <Grid item container xs={12} sm={12} md={6} spacing={6}>
          {data?.map((item: any, key: any) => (
            <CardProducts key={key} post={item} />
          ))}
        </Grid>
      </Grid>
      <Box
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          margin: "2rem",
        }}
      >
        <Pagination
          page={page}
          count={count}
          onChange={handleChangePage}
          color="primary"
          variant="outlined"
        />
      </Box>
    </Box>
  );
};
