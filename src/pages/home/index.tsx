import React from "react";
import { HeaderHome, Categories, Products } from "./lib";
import { useDispatch, useSelector } from "react-redux";
import {
  getCategoires,
  selectCategoies,
  selectLoading,
  getProducts,
  selectLoadingP,
  selectProducts,
  selectSelectedCat,
  selectTotal,
} from "store";
import MuiAlert, { AlertProps } from "@mui/material/Alert";
import { CircularProgress, Skeleton, Box, Snackbar } from "@mui/material";
import "./lib/home.css";

const minDistance = 10;

export const HomePage = () => {
  const dispatch = useDispatch();

  const loading = useSelector(selectLoading);
  const data = useSelector(selectCategoies);
  const products = useSelector(selectProducts);
  var min = Math.min.apply(
    null,
    products.map(function (item: any) {
      return item.price;
    })
  );
  var max = Math.max.apply(
    null,
    products.map(function (item: any) {
      return item.price;
    })
  );
  const laodingP = useSelector(selectLoadingP);
  const selectedCate = useSelector(selectSelectedCat);
  const [color, setColor] = React.useState("");
  const [open, setOpen] = React.useState(false);
  const [fromValue, setFrom] = React.useState("");
  const [toValue, setTo] = React.useState("");
  const [value, setValue] = React.useState<number[]>([min, max]);
  const [star, setStar] = React.useState<number | null>(0);
  const [page, setPage] = React.useState(1);
  const Count = useSelector(selectTotal);

  const Alert = React.forwardRef<HTMLDivElement, AlertProps>(function Alert(
    props,
    ref
  ) {
    return <MuiAlert elevation={6} ref={ref} variant="outlined" {...props} />;
  });
  let reqParams = { categoryId: selectedCate?.id, _page: page, _limit: 10 };
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setColor(event.target.value);
  };
  const handleChangeFrom = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFrom(event.target.value);
  };
  const handleChangeTo = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTo(event.target.value);
  };

  const handleChangeMinMax = (
    event: Event,
    newValue: number | number[],
    activeThumb: number
  ) => {
    if (!Array.isArray(newValue)) {
      return;
    }

    if (activeThumb === 0) {
      setValue([Math.min(newValue[0], value[1] - minDistance), value[1]]);
    } else {
      setValue([value[0], Math.max(newValue[1], value[0] + minDistance)]);
    }
  };
  const onSubmitPrice = () => {
    if (fromValue === "" || toValue === "") {
      setOpen(true);
    } else {
      dispatch(
        getProducts({
          params: { ...reqParams, price_gte: fromValue, price_lte: toValue },
        })
      );
    }
  };

  const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
    if (reason === "clickaway") {
      return;
    }

    setOpen(false);
  };
  const handleChangePage = (
    event: React.ChangeEvent<unknown>,
    value: number
  ) => {
    setPage(value);
  };
  const onSubmitColor = () => {
    if (color === "") {
      dispatch(getProducts({ params: reqParams }));
    } else {
      dispatch(getProducts({ params: { ...reqParams, color: color } }));
    }
  };
  const onSubmitSlider = () => {
    dispatch(
      getProducts({
        params: { ...reqParams, price_gte: value[0], price_lte: value[1] },
      })
    );
  };
  React.useEffect(() => {
    dispatch(getCategoires());
  }, [dispatch]);
  React.useEffect(() => {
    dispatch(getProducts({ params: reqParams }));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, selectedCate?.id, page]);
  return (
    <div className="headerMargin">
      <HeaderHome />
      <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error" sx={{ width: "100%" }}>
          You Should Enter both Values
        </Alert>
      </Snackbar>
      {loading ? (
        <div
          style={{
            alignItems: "center",
            justifyContent: "center",
            display: "flex",
            margin: 20,
          }}
        >
          <CircularProgress color="secondary" />
        </div>
      ) : (
        <Categories data={data} />
      )}
      {laodingP ? (
        <>
          <Skeleton variant="rectangular" width={210} height={118} />
          <Box sx={{ pt: 0.5 }}>
            <Skeleton />
            <Skeleton width="60%" />
          </Box>
        </>
      ) : (
        <Products
          data={products}
          onSub={onSubmitColor}
          colorText={color}
          count={Math.round(Count / 10)}
          page={page}
          valueMinMax={value}
          handleChange={handleChangeMinMax}
          toValue={toValue}
          fromValue={fromValue}
          onChangeFrom={handleChangeFrom}
          onChangeTo={handleChangeTo}
          handleChangePage={handleChangePage}
          onChange={handleChange}
          values={star}
          clearRate={() => {
            dispatch(getProducts({ params: { ...reqParams } }));
            setStar(0);
          }}
          onSubmitSlider={onSubmitSlider}
          submitPrice={onSubmitPrice}
          onChangeStar={(event, newValue) => {
            setStar(newValue);
            dispatch(
              getProducts({ params: { ...reqParams, rating: newValue } })
            );
          }}
        />
      )}
    </div>
  );
};
