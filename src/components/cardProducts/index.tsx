import * as React from "react";
import Typography from "@mui/material/Typography";
import { Card, Rating, CardContent, CardMedia, Grid } from "@mui/material";
import { Box } from "@mui/system";

interface ProductsProps {
  post: {
    name: string;
    color: string;
    image: string;
    price: string;
    currency: string;
    releaseDate: string;
    rating: number | any;
    categoryId: string;
  };
}

export const CardProducts = (props: ProductsProps) => {
  const { post } = props;
  const fromTextToSymbol = () => {
    if (post.currency === "USD") {
      return "$";
    } else {
      return "EGY";
    }
  };
  return (
    <Grid item xs={6}>
      <Card sx={{ maxWidth: 345, height: 450 }}>
        <CardMedia
          component="img"
          sx={{ width: "100%", display: { xs: "none", sm: "block" } }}
          image={post.image}
          alt="post.imageLabel"
        />
        <CardContent>
          <Typography component="h2" variant="h5">
            {post?.name}
          </Typography>
          <Rating name="disabled" value={post.rating} readOnly />
          <Typography variant="subtitle1" color="text.secondary">
            {post?.color}
          </Typography>
          <Box sx={{ display: "flex", flexDirection: "row" }}>
            <Typography sx={{ paddingRight: 1 }}>
              {fromTextToSymbol()}
            </Typography>
            <Typography variant="subtitle1" paragraph>
              {post?.price}
            </Typography>
          </Box>
          <Typography variant="subtitle1" color="primary">
            Continue...
          </Typography>
        </CardContent>
      </Card>
    </Grid>
  );
};
