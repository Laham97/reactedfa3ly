import React from "react";
import { TextField, Box, Slider, Button } from "@mui/material";
interface Props {
  labelOne: string;
  labelTow: string;
  variantOne: "outlined" | "filled" | "standard";
  variantTow: "outlined" | "filled" | "standard";
  value: number[];
  handleChange: (e: Event, news: number | number[]) => void;
  min?: number;
  max?: number;
  fromValue: string;
  onChangeFrom: (event: React.ChangeEvent<HTMLInputElement>) => void;
  toValue: string;
  onChangeTo: (event: React.ChangeEvent<HTMLInputElement>) => void;
  submitPrice: () => void;
  onSubmitSlider: () => void;
}
export const PriceRange: React.FC<Props> = ({
  labelOne,
  labelTow,
  variantOne,
  variantTow,
  value,
  handleChange,
  min,
  max,
  fromValue,
  onChangeFrom,
  toValue,
  onChangeTo,
  submitPrice,
  onSubmitSlider,
}) => {
  return (
    <Box>
      <Box
        sx={{
          display: "flex",
          justifyContent: "space-between",
          padding: 2,
        }}
      >
        <TextField
          size="small"
          id="outlined-basic"
          label={labelOne}
          sx={{ width: 120, marginRight: 10 }}
          variant={variantOne}
          value={fromValue}
          onChange={onChangeFrom}
        />
        <TextField
          size="small"
          id="outlined-basic1"
          label={labelTow}
          sx={{ width: 120 }}
          variant={variantTow}
          value={toValue}
          onChange={onChangeTo}
        />
        <Button sx={{ marginLeft: 10 }} onClick={submitPrice}>
          Submit Price
        </Button>
      </Box>
      <Box sx={{ width: 300 }}>
        <Slider
          value={value}
          onChange={handleChange}
          valueLabelDisplay="auto"
          max={max}
          min={min}
          onChangeCommitted={onSubmitSlider}
        />
      </Box>
    </Box>
  );
};
