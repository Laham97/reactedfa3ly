import * as React from "react";
import Rating from "@mui/material/Rating";

interface Props {
  value: number | null;
  onChangeStar: (event: any, newValue: any | null) => void;
}

export const RadioGroupRating: React.FC<Props> = ({ onChangeStar, value }) => {
  return (
    <Rating name="simple-controlled" value={value} onChange={onChangeStar} />
  );
};
