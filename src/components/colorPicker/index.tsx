import React from "react";
import { TextField, Box, Checkbox, Typography } from "@mui/material";

interface Props {
  data: any;
  colorText: string;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onSub: () => void;
}
export const ColorPicker: React.FC<Props> = ({
  data,
  colorText,
  onChange,
  onSub,
}) => {
  return (
    <Box>
      <TextField
        size="small"
        id="outlined-basic"
        label="Enter Color"
        value={colorText}
        onChange={onChange}
        onBlur={onSub}
        sx={{
          width: 120,
          margin: 2,
        }}
        variant="outlined"
      />
      <Box sx={{ overflowY: "scroll", height: 250 }}>
        {data?.map((item: any, key: any) => (
          <Box
            sx={{
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
              margin: 2,
            }}
            key={key}
          >
            <Checkbox
              key={key}
              sx={{
                color: item,
                "&.Mui-checked": {
                  color: item,
                },
              }}
            />
            <Typography>{item}</Typography>
          </Box>
        ))}
      </Box>
    </Box>
  );
};
