import { applyMiddleware, createStore, combineReducers, compose } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import Thunk from "redux-thunk";
import storage from "redux-persist/lib/storage";

import CategorieReducer from "./Categories/categories.slice";
import ProductsReducer from "./Products/products.slice";

const persistConfig = {
  key: "root",
  storage,
  whitelist: ["categoires"],
};

const rootReducer = combineReducers({
  categoires: CategorieReducer,
  product: ProductsReducer,
});
export type RootState = ReturnType<typeof rootReducer>;

const reactotron = require("../config/reactotron").default;
const reactotronMiddleware = reactotron.createEnhancer();
let persistedReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(
  persistedReducer,
  compose(applyMiddleware(Thunk), reactotronMiddleware)
);

const persistor = persistStore(store);

export { store, persistor };
