import { createSlice } from "@reduxjs/toolkit";
import { RootState } from "../store";
import { getProducts } from "./products.action";

interface productsState {
  loading: boolean;
  products: any;
  totalCount: number;
}
const initialState: productsState = {
  loading: false,
  products: [],
  totalCount: 0,
};

export const productSlice = createSlice({
  name: "product",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(getProducts.fulfilled, (state, action) => {
      state.products = action.payload?.data;
      state.loading = false;
      state.totalCount = action.payload?.headers["x-total-count"];
    });

    builder.addCase(getProducts.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getProducts.rejected, (state) => {
      state.loading = false;
    });
  },
});

export const selectLoadingP = (state: RootState) => state.product.loading;
export const selectProducts = (state: RootState) => state.product.products;
export const selectTotal = (state: RootState) => state.product.totalCount;
export default productSlice.reducer;
