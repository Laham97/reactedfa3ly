import { createAsyncThunk } from "@reduxjs/toolkit";
import { mockApiP } from "api";

export const getProducts = createAsyncThunk<any, any, any>(
  "[GET] /PRODUCTS",
  async (args, { rejectWithValue }) => {
    try {
      const response: any = await mockApiP.get(args);
      if (response?.data?.message) {
        throw response.message;
      } else if (response?.data === null) {
        throw console.log("A");
      }
      return response;
    } catch (e) {
      return rejectWithValue(e);
    }
  }
);
