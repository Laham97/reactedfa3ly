import { createSlice } from "@reduxjs/toolkit";
import { RootState } from "../store";
import { getCategoires } from "./categries.action";
// import { toast } from "react-toastify";

interface categoiresState {
  loading: boolean;
  categoires: any;
  selectedCat: {
    name: string | undefined;
    id: number | undefined;
    image: string | undefined;
  };
}
const initialState: categoiresState = {
  loading: false,
  categoires: [],
  selectedCat: {
    name: undefined,
    id: undefined,
    image: undefined,
  },
};

export const categoiresSlice = createSlice({
  name: "categoires",
  initialState,
  reducers: {
    setCategoire: (state, action) => {
      state.selectedCat = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(getCategoires.fulfilled, (state, action) => {
      state.categoires = action.payload;
      state.selectedCat = {
        name: action.payload[0]?.name,
        id: action.payload[0]?.id,
        image: action.payload[0]?.image,
      };
      state.loading = false;
    });

    builder.addCase(getCategoires.pending, (state) => {
      state.loading = true;
    });
    builder.addCase(getCategoires.rejected, (state) => {
      state.loading = false;
    });
  },
});

export const { setCategoire } = categoiresSlice.actions;

export const selectLoading = (state: RootState) => state.categoires.loading;
export const selectCategoies = (state: RootState) =>
  state.categoires.categoires;
export const selectSelectedCat = (state: RootState) =>
  state.categoires.selectedCat;
export default categoiresSlice.reducer;
