import { createAsyncThunk } from "@reduxjs/toolkit";
import { mockApiC } from "api";

export const getCategoires = createAsyncThunk("[GET] /CATEGORIE", async () => {
  try {
    const response: any = await mockApiC.get();
    if (response?.data?.message) {
      throw response.message;
    } else if (response?.data === null) {
      throw console.log("A");
    }
    return response.data;
  } catch (e) {
    return e;
  }
});
